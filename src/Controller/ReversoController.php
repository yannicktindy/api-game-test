<?php

namespace App\Controller;

use App\Entity\Reverso;
use App\Entity\Revcard;
use App\Repository\RevcardRepository;
use App\Repository\ReversoRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ReversoController extends AbstractController
{
    #[Route('/reverso', name: 'app_reverso')]
    public function index(
        EntityManagerInterface $entityManager,
        ReversoRepository $reversoRepository,
        RevcardRepository $revcardRepository
    ): Response
    {
        $reverso = $reversoRepository->find(1);
        // find card by reverso id 1
        $revcards = $revcardRepository->findBy(['reverso' => $reverso]);
        $revcards = array_merge($revcards, $revcards);
        shuffle($revcards);
        return $this->render('reverso/index.html.twig', [
            'reverso' => $reverso,
            'revcards' => $revcards,

        ]);
    }
}
