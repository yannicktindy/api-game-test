<?php

namespace App\Controller;

use App\Entity\Visit;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CVController extends AbstractController
{
    #[Route('/', name: 'app_cv')]
    public function index(): Response
    {
        $newVisit = new Visit();
        $newVisit->setUpdatedAt(new \DateTime('now'));
        
        return $this->render('cv/index.html.twig', [
            'controller_name' => 'CVController',
        ]);
    }
}
