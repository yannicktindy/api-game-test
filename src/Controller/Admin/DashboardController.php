<?php

namespace App\Controller\Admin;

use App\Repository\UserRepository;
use App\Repository\VisitRepository;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class DashboardController extends AbstractDashboardController
{
    public function __construct(
        private VisitRepository $visitRepository,
        private UserRepository $userRepository
    ){}
    
    #[Route('/admin', name: 'admin')]
    #[IsGranted('ROLE_SUPER_ADMIN')]
    public function index(): Response
    {
        $allVisits = $this->visitRepository->findAll();
        $allUsers = $this->userRepository->findAll();

        return $this->render('admin/index.html.twig', [
            'allVisits' => $allVisits,
            'allUsers' => $allUsers
        ]);
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('NewSilenus');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToRoute('Retour à l\'accueil', 'fa fa-home', 'app_cv');
        yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');

        yield MenuItem::subMenu('Game Reverso', 'fa-solid fa-cubes')->setSubItems([
            MenuItem::linkToCrud('Reverso', 'fas fa-eye', Reverso::class)
                ->setController(ReversoCrudController::class),
            MenuItem::linkToCrud('Revcard', 'fas fa-eye', Revcard::class)
                ->setController(RevcardCrudController::class),   
        ]);

        // yield MenuItem::linkToCrud('The Label', 'fas fa-list', EntityClass::class);
    }
}
