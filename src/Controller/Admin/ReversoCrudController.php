<?php

namespace App\Controller\Admin;

use App\Entity\Reverso;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ReversoCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Reverso::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            TextField::new('name'),
            TextField::new('img'),
            CollectionField::new('cards')->hideOnForm(),
        ];
    }   
}
