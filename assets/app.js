/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css';

// start the Stimulus application
import './bootstrap';
// import './js/dayNight.js';
import * as bootstrap from 'bootstrap';
console.log('pageModal ok');
// import * as bootstrap from 'bootstrap'

window.onload = setTimeout(function() {
    const modalTest = new bootstrap.Modal('#pageModal');
    modalTest.show();
}, 500);

